
# -*- coding: utf-8 -*-
# By  - Bessam Djerad - www.getuon.com
{
	"name": "Amount In Words",
    "summary": """
	This module print the total amount in words, in Quotation, Sale Order, Pro-format and invoice, in the partner language.
	""",
    "category": "Point of Sale",
    "images": ["images/inwords.png"],
    "version": "1.0",
    "author": "Bessam Djerad",
    "support": "bessam.dj@gmail.com",
    "license": "LGPL-3",
    "price": 15.00,
    "currency": "EUR",

    "depends": [
        "point_of_sale",
   ],
    "external_dependencies": {"python": [], "bin": []},
    "data": [
        "views/inwords.xml",
    ],


    "auto_install": False,
    "installable": True,
    "application": False,

}